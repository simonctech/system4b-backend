<?php

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
// Esta clase es creada como base para los campos de Auditoría

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BaseEntity
{
    /**
     * Status [0:Inactive, 1:Active]
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    protected $status = 1;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt = NULL;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $removed = FALSE;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatusText(): ?string
    {
        switch ($this->getStatus()) {
            default:
                $text = 'Not indicated';
                break;
            case 0:
                $text = 'Inactive';
                break;
            case 1:
                $text = 'Active';
                break;
        }

        return $text;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     * https://symfony.com/doc/current/doctrine/events.html#doctrine-lifecycle-callbacks
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getRemoved(): ?bool
    {
        return $this->removed;
    }

    public function setRemoved(bool $removed): self
    {
        $this->removed = $removed;

        return $this;
    }
}
