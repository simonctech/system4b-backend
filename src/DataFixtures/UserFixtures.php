<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 */
class UserFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail("admin@system4book.com");
        $user->setName("Super Admin");
        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            'Admin@System4Book.'
        ));
        
        $manager->persist($user);
        $manager->flush();
    }
}
