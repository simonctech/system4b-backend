<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This class works as the Base for all API Controllers
 * 
 * Control endpoint responses
 * Response with code: 200, 201, 204, 400, 401, 403, 404, 500
 * 
 * Methods to pre-format the data
 * 
 * https://blog.restcase.com/5-basic-rest-api-design-guidelines/
 * https://developer.okta.com/blog/2018/06/14/php-crud-app-symfony-vue
 */
class BaseAPIController extends AbstractController
{
    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = 200;

    protected $em;
    protected $serializer;

    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Sets the api response format.
     *
     * @param array|object|null $data
     * @param array $msg
     *
     * @return array
     */
    private function setResponseFormat($data = null, $msg = [], $err = false)
    {
        $response = [];
        $response['statusCode'] = $this->getStatusCode();
        $response['statusText'] = $this->getStatusCode() < 400 ? "successful" : "error";
        $response['data'] = !$err ? $data : null;
        $response['errors'] = $err ? $data : null;
        $response['error'] = !($this->getStatusCode() < 400);
        $response['messages'] = $msg ?? ["successful query!"];

        return $response;
    }

    /**
     * Sets data serialization.
     *
     * @param array|object|null $data
     *
     * @return array
     */
    private function setDataSerialization($data = null)
    {
        $data = $this->serializer->serialize($data, 'json');

        return $data;
    }

    /**
     * Generic method for responses 200 (ONLY FOR TESTS).
     *
     * @param array|object|null $data
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonResponse($data, $headers = [])
    {
        $this->setStatusCode(200);

        $response = $this->setResponseFormat($data);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns successful requests.
     *
     * @param array|object|null $data
     * @param int $code
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonResponseSuccess($data, $code = 200, $msg = [], $headers = [])
    {
        $this->setStatusCode($code);

        $response = $response = $this->setResponseFormat($data, $msg);

        if ($code == 201) {
            $response['messages'][] = "record created!";
        }

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns bad request errors.
     *
     * @param array|object|null $data
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonResponseBadRequest($data, $msg = [], $headers = [])
    {
        $this->setStatusCode(400);

        if (empty($msg)) {
            $msg[] = "Error, please enter missing parameter(s)";
        }

        $response = $response = $this->setResponseFormat($data, $msg);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns unauthorized access errors.
     * Invalid authentication credentials.
     *
     * @param array|object|null $data
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonRespondUnauthorized($data, $msg = [], $headers = [])
    {
        $this->setStatusCode(401);

        if (empty($msg)) {
            $msg[] = "Not authorized! Invalid authentication credentials!";
        }

        $response = $response = $this->setResponseFormat($data, $msg);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns access forbidden errors.
     *
     * @param array|object|null $data
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonRespondForbidden($data, $msg = [], $headers = [])
    {
        $this->setStatusCode(403);

        if (empty($msg)) {
            $msg[] = "Sorry, you do not have permission to access this resource!";
        }

        $response = $response = $this->setResponseFormat($data, $msg);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns record not found errors.
     *
     * @param array|object|null $data
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonRespondNotFound($data, $msg = [], $headers = [])
    {
        $this->setStatusCode(404);

        if (empty($msg)) {
            $msg[] = "Not found!";
        }

        $response = $response = $this->setResponseFormat($data, $msg);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }

    /**
     * Generic method that handles and returns errors.
     *
     * @param array|object|null $errors
     * @param array $msg
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function jsonRespondError($errors, $msg = [], $headers = [])
    {
        $this->setStatusCode(500);

        if (empty($msg)) {
            $msg[] = "Internal Server Error!";
        }

        $response = $response = $this->setResponseFormat($errors, $msg, true);

        $response = $this->setDataSerialization($response);

        return new JsonResponse($response, $this->getStatusCode(), $headers, true);
    }
}
