<?php

namespace App\Controller;

use App\Controller\BaseAPIController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Example2Controller extends BaseAPIController
{
    /**
     * @Route(
     *     name="get_example",
     *     path="/list",
     *     methods={"GET"},
     *     defaults={"_api_item_operation_name"="get_example"}
     * )
     */
    public function list(Request $request)
    {
        try {
            $params = $request->query->all();
            $records = $this->em->getRepository("App:Example2")->findAll();
        } catch (Exception $ex) {
            return $this->jsonRespondError($ex, 'exception');
        }

        return $this->jsonResponseSuccess($records);
    }
}
